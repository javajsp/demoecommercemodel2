/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;
import java.util.List;

/**
 *
 * @author Setup
 */
public class ProductFinderBean {
    private String keyword;
    
    public void setKeyword(String keyword)
    {this.keyword = this.keyword;}
    public List<Product> getProducts()
    {
        return new
            ProductDataAccess().getProductsByName(keyword);
    }
}
